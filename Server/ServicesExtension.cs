﻿using KPI.DB.Server.Services;

using Microsoft.Extensions.DependencyInjection;

namespace KPI.DB.Server
{
    public static class ServicesExtension
    {
        public static void AddKPIDBServices(this IServiceCollection services)
        {
            services.AddTransient<IDatabaseInitializationService, DatabaseInitializationService>();
        }
    }
}
