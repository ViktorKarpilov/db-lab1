﻿using KPI.DB.Persistance.Repositories;

using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic.FileIO;

using System.Threading.Tasks;

namespace KPI.DB.Server.Services
{
    public class DatabaseInitializationService : IDatabaseInitializationService
    {
        private readonly IDatabaseInitializationRepository _databaseInitializationRepository;
        private readonly IConfiguration _iConfig;

        public DatabaseInitializationService(IDatabaseInitializationRepository databaseInitializationRepository,
                                             IConfiguration iConfig)
        {
            this._databaseInitializationRepository = databaseInitializationRepository;
            this._iConfig = iConfig;
        }

        public async Task LoadYearsDatasets(int[] yeasrs)
        {
            foreach(int year in yeasrs)
            {
                string fileName = _iConfig.GetSection("Datasets").GetValue<string>($"{year}");

                using TextFieldParser parser = new TextFieldParser($"../Dataset/{fileName}")
                {
                    TextFieldType = FieldType.Delimited
                };
                parser.SetDelimiters(";");
                long uploadedPersons = await _databaseInitializationRepository.GetNumberOfUploadedRecords(year);
                await _databaseInitializationRepository.LoadYear(parser, year, uploadedPersons);
            }
        }

        public async Task<long> GetCountOFRecords()
        {
            return await _databaseInitializationRepository.GetNumberOfUploadedRecords(2020) + await _databaseInitializationRepository.GetNumberOfUploadedRecords(2019);
        }

        public double? YearSubjectAvarage(int year, string subject)
        {
            return _databaseInitializationRepository.GetYearSubjectAvarage(year, subject);
        }
    }

    public interface IDatabaseInitializationService
    { 
        public Task<long> GetCountOFRecords();
        public Task LoadYearsDatasets(int[] yeasrs);
        public double? YearSubjectAvarage(int year, string subject);
    }
}
