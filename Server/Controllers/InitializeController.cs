﻿using KPI.DB.Server.Services;

using Microsoft.AspNetCore.Mvc;

using System.Threading.Tasks;

namespace KPI.DB.Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InitializeController : ControllerBase
    {
        private readonly IDatabaseInitializationService _databaseInitializationService;

        public InitializeController(IDatabaseInitializationService databaseInitializationService)
        {
            this._databaseInitializationService = databaseInitializationService;
        }
        [HttpGet("{year}")]
        public async Task<OkResult> UploadYearRecords([FromRoute]int year)
        {
            await _databaseInitializationService.LoadYearsDatasets(new[] { year });
            return Ok();
        }

        [HttpGet]
        public async Task<long> GetCountOfRecords()
        {
            return await _databaseInitializationService.GetCountOFRecords();
        }

        [HttpGet("{subject}/{year}")]
        public double? GetSubjectAvarageForYear([FromRoute] string subject, [FromRoute] int year)
        {
            return _databaseInitializationService.YearSubjectAvarage(year, subject);
        }
    }
}
