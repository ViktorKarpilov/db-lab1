﻿using KPI.DB.Domain.Models;
using KPI.DB.Domain.Patterns;

using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic.FileIO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KPI.DB.Persistance.Repositories
{
    public class DatabaseInitializationRepository : IDatabaseInitializationRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly List<Subject> _subjects;

        public DatabaseInitializationRepository(ApplicationDbContext _context)
        {
            this._context = _context;
            _subjects = _context.Subjects.ToList();
        }

        public async Task<long> GetNumberOfUploadedRecords(int year)
        {
            Count count = (await _context.Counter.SingleOrDefaultAsync(counter => counter.Year == year));

            return count is null ? 0 : count.PersonsCount;
        }

        public async Task LoadYear(TextFieldParser parser, int year, long startingRecord = 1)
        {
            List<string> fields = parser.ReadFields().ToList();

            int identifierId = fields.IndexOf("OUTID");

            IDictionary<string, KeyValuePair<int, int>> subjectsResults = GetSubjectsIndexes(fields);

            long linesAddingCounter = 0;

            for (int i = 1; i < startingRecord; i++)
            {
                parser.ReadFields();
            }

            while (!parser.EndOfData)
            {
                string[] fieldsArray = parser.ReadFields();

                Person person = new Person() { Identifier = new Guid(fieldsArray[identifierId]), Results = new List<Result>() };

                foreach (string subject in subjectsResults.Keys)
                {
                    Result result = new Result();

                    if (fieldsArray[subjectsResults[subject].Value] != "null")
                    {
                        result.Score = Convert.ToInt32(fieldsArray[subjectsResults[subject].Value].Remove(Convert.ToInt32(fieldsArray[subjectsResults[subject].Value].IndexOf(','))));
                    }
                    else
                    {
                        result.Score = null;
                    }

                    //`(*>﹏<*)′
                    result.IsPassed = ToUTF32(fieldsArray[subjectsResults[subject].Key]).Length == "Зараховано".Length ? true : false;

                    Subject currentSubject = _context.Subjects.Single(dbSubject => dbSubject.SubjectName == subject);

                    result.Subject = currentSubject;
                    result.Year = year;

                    person.Results.Add(result);
                }

                await _context.Persons.AddAsync(person);

                linesAddingCounter++;

                if (linesAddingCounter % 100 == 0)
                {
                    startingRecord += linesAddingCounter;
                    linesAddingCounter = 0;

                    Count count = await _context.Counter.FirstOrDefaultAsync(count => count.Year == year);

                    if (count is not null)
                    {
                        count.PersonsCount = startingRecord;
                    }
                    else
                    {
                        count = new Count { PersonsCount = startingRecord, Year = year };
                        await _context.Counter.AddAsync(count);
                    }        

                    await _context.SaveChangesAsync();

                    Console.WriteLine("100 lines was inserted and saved");
                }
            }
        }


        private string ToUTF32(string input)
        {
            Encoding encoding = Encoding.UTF32;
            Encoding encodingUtf = Encoding.UTF8;

            byte[] unicodeBytes = encodingUtf.GetBytes(input);

            byte[] encodingBytes = Encoding.Convert(encodingUtf, encoding, unicodeBytes);

            // Convert the new byte[] into a char[] and then into a string.
            char[] asciiChars = new char[encoding.GetCharCount(encodingBytes, 0, encodingBytes.Length)];
            encoding.GetChars(encodingBytes, 0, encodingBytes.Length, asciiChars, 0);
            string result = new string(asciiChars);

            return result;
        }

        //<subjectName,<positionOfStatus,positionOfPoints>>
        private IDictionary<string, KeyValuePair<int, int>> GetSubjectsIndexes(List<string> fields)
        {
            IDictionary<string, KeyValuePair<int, int>> result = new Dictionary<string, KeyValuePair<int, int>>();

            Regex statusRegExp = new Regex(Patterns.TestStatusPattern);

            foreach (string field in fields)
            {
                if (statusRegExp.IsMatch(field))
                {
                    var subject = new Subject() { SubjectName = field.Replace("TestStatus", "") };

                    if (!_subjects.Contains(subject))
                    {
                        Subject treckedSubject = _context.Add(subject).Entity;
                        _subjects.Add(treckedSubject);
                    }

                    result.Add(subject.SubjectName, new KeyValuePair<int, int>(fields.IndexOf(field), fields.IndexOf(field) + 1));
                }
            }

            _context.SaveChanges();
            return result;
        }

        public double? GetYearSubjectAvarage(int year,string subject)
        {
            Subject dbSubject = _context.Subjects.FirstOrDefault(x => x.SubjectName == subject);
            List<Result> results = _context.Results.Where(res => res.Year == year && res.Score > 0 && res.Subject == dbSubject).ToList();
            double? result = results.Average(res => res.Score);
            return result;
        }

    }

    public interface IDatabaseInitializationRepository
    {
        public Task LoadYear(TextFieldParser parser, int year, long startingRecord = 1);
        public Task<long> GetNumberOfUploadedRecords(int year);
        public double? GetYearSubjectAvarage(int year, string subject);
    }
}
