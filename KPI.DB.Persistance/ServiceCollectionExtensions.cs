﻿using KPI.DB.Persistance.Repositories;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace KPI.DB.Persistance
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddPersistenceServices(this IServiceCollection services,
                                                                IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                string connectionString = configuration.GetConnectionString("ApplicationDbContext");

                options
                   .UseNpgsql(connectionString)
                   .UseSnakeCaseNamingConvention();

                options.EnableSensitiveDataLogging();
            });
            services.AddRepositories();

            return services;
        }

        private static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IDatabaseInitializationRepository, DatabaseInitializationRepository>();
        }
    }
}
