﻿using KPI.DB.Domain;
using KPI.DB.Domain.Models;

using Microsoft.EntityFrameworkCore;

namespace KPI.DB.Persistance
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Result> Results { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Count> Counter { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Count>().HasKey(count => count.Year);
            modelBuilder.Entity<Person>()
                .HasKey(person => person.Identifier);
        }
    }
}
