﻿using System;
using System.Collections.Generic;

namespace KPI.DB.Domain.Models
{
    public class Person
    {
        public Guid Identifier { get; set; }
        public ICollection<Result> Results { get; set;}
    }
}
