﻿using System;
using System.Collections.Generic;

namespace KPI.DB.Domain.Models
{
    public class Subject : BaseEntity, IEquatable<Subject>
    {
        public string SubjectName { get; set; }

        public override bool Equals(object obj)
        {
            return Equals(obj as Subject);
        }

        public bool Equals(Subject other)
        {
            return other != null &&
                   SubjectName == other.SubjectName;
        }

        public override int GetHashCode()
        {
            return -906441044 + EqualityComparer<string>.Default.GetHashCode(SubjectName);
        }
    }
}
