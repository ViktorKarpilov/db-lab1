﻿using FluentMigrator;

namespace KPI.DB.Database.Migrations.Migrations
{
    [Migration(202102141750)]
    public class Initialization : Migration
    {
        private const string PersonsTableName = "persons";
        private const string ReultssTableName = "results";
        private const string SubjectsTableName = "subjects";

        private const string CounterName = "counter";

        public override void Down()
        {
            Delete.Table(PersonsTableName);
            Delete.Table(ReultssTableName);
            Delete.Table(SubjectsTableName);
            Delete.Table(CounterName);
        }

        public override void Up()
        {
            Create.Table(PersonsTableName)
                .WithColumn("identifier")
                .AsGuid();

            Create.Table(SubjectsTableName)
                .WithColumn("id")
                .AsInt32()
                .PrimaryKey().Identity()
                .WithColumn("subject_name")
                .AsString();

            Create.Table(ReultssTableName)
                .WithColumn("id")
                    .AsInt32().PrimaryKey().Identity()
                .WithColumn("subject_id")
                    .AsInt32()
                .WithColumn("person_identifier")
                    .AsGuid()
                .WithColumn("score")
                    .AsInt32()
                    .Nullable()
                .WithColumn("is_passed")
                    .AsBoolean()
                    .WithDefaultValue("false")
                    .WithColumn("year")
                    .AsInt32();

            Create.Table(CounterName)
                .WithColumn("year")
                .AsInt32()
                .WithColumn("persons_count")
                .AsInt64()
                .WithDefaultValue(0);
        }
    }
}
